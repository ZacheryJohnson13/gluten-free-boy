﻿using UnityEngine;
using System.Collections;

public class CharacterShield : MonoBehaviour {
		
	void FixedUpdate(){
		renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, renderer.material.color.a - (Time.deltaTime * .15f));
	}
}
