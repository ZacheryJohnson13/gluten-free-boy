﻿using UnityEngine;
using System.Collections;

public class MainMenuButton : MonoBehaviour {

	public void OnPress(){
		GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().MoveCamera(GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().gameCameraInitialPosition);
	}
}