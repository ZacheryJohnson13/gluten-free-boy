﻿using UnityEngine;
using System.Collections;

public class MenuToggle : MonoBehaviour {
	public bool on = false;

	public void Update(){
		if(on){
			GetComponent<TextMesh>().text = "On";
		}
		else if(!on){
			GetComponent<TextMesh>().text = "Off";
		}
	}

	public void Toggle(){
		on = !on;
	}
}