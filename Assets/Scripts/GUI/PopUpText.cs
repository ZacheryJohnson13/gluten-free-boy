﻿using UnityEngine;
using System.Collections;

public class PopUpText : MonoBehaviour {

	void Awake(){
		GetComponent<TextMesh>().color = new Color(255, 255, 0, .35f);
	}

	void Update () {
		Color targetColor = GetComponent<TextMesh>().color;
		targetColor.a -= Time.deltaTime/5;
		GetComponent<TextMesh>().color = targetColor;

		transform.position = Vector3.Slerp(transform.position, new Vector3(transform.position.x, transform.position.y + .75f, transform.position.z), Time.deltaTime);

		if(GetComponent<TextMesh>().color.a < .05f){
			Destroy(gameObject);
		}
	}

	public void Set(string _text){
		GetComponent<TextMesh>().text = _text;
	}
}
