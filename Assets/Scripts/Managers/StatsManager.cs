﻿using UnityEngine;
using System.Collections;

public class StatsManager : MonoBehaviour {
	public int totalScore;
	public int highScore;
	public int gamesPlayed;

	public int lasersShot;
	public int lasersHit;
	public int laserAccuracy;

	public int asteroidsDestroyed;
	public int asteroidsHit;
	public int asteroidsEncountered;

	void Awake(){
		LoadStats();
	}

	public void LoadStats(){
		totalScore = PlayerPrefs.GetInt("Total Score");
		highScore = PlayerPrefs.GetInt("High Score");
		gamesPlayed = PlayerPrefs.GetInt("Games Played");

		lasersShot = PlayerPrefs.GetInt("Lasers Shot");
		lasersHit = PlayerPrefs.GetInt("Lasers Hit");
		if(lasersShot != 0){
			laserAccuracy = Mathf.RoundToInt(((float)lasersHit/(float)lasersShot) * 100);
		}
		else{
			laserAccuracy = 0;
		}
		asteroidsDestroyed = PlayerPrefs.GetInt("Asteroids Destroyed");
		asteroidsHit = PlayerPrefs.GetInt("Asteroids Hit");
		asteroidsEncountered = PlayerPrefs.GetInt("Asteroids Encountered");
	}

	public void SaveAllStats(){
		PlayerPrefs.SetInt("Total Score", totalScore);
		PlayerPrefs.SetInt("High Score", highScore);
		PlayerPrefs.SetInt("Games Played", gamesPlayed);

		PlayerPrefs.SetInt("Lasers Shot", lasersShot);
		PlayerPrefs.SetInt("Lasers Hit", lasersHit);
		PlayerPrefs.SetInt("Laser Accuracy", laserAccuracy);

		PlayerPrefs.SetInt("Asteroids Destroyed", asteroidsDestroyed);
		PlayerPrefs.SetInt("Asteroids Hit", asteroidsHit);
		PlayerPrefs.SetInt("Asteroids Encountered", asteroidsEncountered);
	}

	public void SubmitScore(int score){
		if(score > highScore){
			highScore = score;
		}

		SaveAllStats();
		LoadStats();
	}
}
