﻿using UnityEngine;
using System.Collections;

public class AppManager : MonoBehaviour {
	public InputManager inputManager;
	public GUIManager guiManager;
	public GameplayManager gameplayManager;

	public int soundEffectVolume;
	public int musicVolume;

	void Awake(){
		DontDestroyOnLoad(gameObject);

		inputManager = GetComponent<InputManager>();
		guiManager = GetComponent<GUIManager>();
		gameplayManager = GetComponent<GameplayManager>();
	}
}
