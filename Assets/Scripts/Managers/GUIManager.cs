﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIManager : MonoBehaviour {
	public GameplayManager gameplayManager;
	public InputManager inputManager;
	public AppManager appManager;
	public StatsManager statsManager;

	public List<GameObject> guiElements = new List<GameObject>();

	public GUIText scoreTicker;
	public GUIText laserCounter;

	public GameObject popUpText;

	public GameObject postGameText;

	public SpriteRenderer options_tilt;
	private Color options_tilt_StartColor;
	public SpriteRenderer options_touch;
	private Color options_touch_StartColor;

	public bool fadeGUI = false;

	public TextMesh stats_CategoryHeader;

	public TextMesh stats_FirstStatLabel;
	public TextMesh stats_SecondStatLabel;
	public TextMesh stats_ThirdStatLabel;

	public TextMesh stats_FirstStatValue;
	public TextMesh stats_SecondStatValue;
	public TextMesh stats_ThirdStatValue;

	public TextMesh postGame_Score;
	public TextMesh postGame_LasersShot;
	public TextMesh postGame_AsteroidsDestroyed;

	public int statMenuCounter = 0;

	void Awake(){
		gameplayManager = GetComponent<GameplayManager>();
		inputManager = GetComponent<InputManager>();
		appManager = GetComponent<AppManager>();
		statsManager = GetComponent<StatsManager>();

		options_tilt_StartColor = options_tilt.color;
		options_touch_StartColor = options_touch.color;
	}

	void Start(){
		UpdateStats();

		guiElements.AddRange(GameObject.FindGameObjectsWithTag("GUI"));

		foreach(GameObject go in guiElements){
			go.renderer.sortingLayerName = "GUI";
		}

		scoreTicker.pixelOffset = new Vector2(Screen.width * .49f, Screen.height * .47f);
		scoreTicker.fontSize = (Screen.height * 15)/ 480;

		laserCounter.pixelOffset = new Vector2(-Screen.width * .49f, Screen.height * .4975f);
		laserCounter.fontSize = (Screen.height * 15) / 480;
	}

	void Update(){
		if(gameplayManager.gameActive){
			scoreTicker.text = Mathf.CeilToInt(gameplayManager.score).ToString();
			laserCounter.text = gameplayManager.character.lasersAvailable.ToString();

			postGame_Score.text = Mathf.CeilToInt(gameplayManager.score).ToString();
			postGame_LasersShot.text = gameplayManager.lasersShot.ToString();
			postGame_AsteroidsDestroyed.text = gameplayManager.asteroidsDestroyed.ToString();
		}

		if(fadeGUI){
			scoreTicker.color = new Color(scoreTicker.color.r, scoreTicker.color.g, scoreTicker.color.b, Mathf.Lerp(scoreTicker.color.a, 1, Time.deltaTime * 1.25f));
			laserCounter.color = new Color(laserCounter.color.r, laserCounter.color.g, laserCounter.color.b, Mathf.Lerp(laserCounter.color.a, 1, Time.deltaTime * 1.25f));
		}
	}

	public void UpdateStats(){
		if(statMenuCounter > 2){
			statMenuCounter = 0;
		}
		if(statMenuCounter < 0){
			statMenuCounter = 2;
		}

		statsManager.LoadStats();

		if(statMenuCounter == 0){
			stats_CategoryHeader.text = "General";

			stats_FirstStatLabel.text = "Total Score";
			stats_SecondStatLabel.text = "High Score";
			stats_ThirdStatLabel.text = "Games Played";

			stats_FirstStatValue.text = statsManager.totalScore.ToString();
			stats_SecondStatValue.text = statsManager.highScore.ToString();
			stats_ThirdStatValue.text = statsManager.gamesPlayed.ToString();
		}
		if(statMenuCounter == 1){
			stats_CategoryHeader.text = "Lasers";

			stats_FirstStatLabel.text = "Lasers Shot";
			stats_SecondStatLabel.text = "Lasers Hit";
			stats_ThirdStatLabel.text = "Laser Accuracy";

			stats_FirstStatValue.text = statsManager.lasersShot.ToString();
			stats_SecondStatValue.text = statsManager.lasersHit.ToString();
			stats_ThirdStatValue.text = statsManager.laserAccuracy.ToString() + "%";
		}
		if(statMenuCounter == 2){
			stats_CategoryHeader.text = "Asteroids";

			stats_FirstStatLabel.text = "Asteroids Destroyed";
			stats_SecondStatLabel.text = "Asteroids Hit";
			stats_ThirdStatLabel.text = "Asteroids Encountered";

			stats_FirstStatValue.text = statsManager.asteroidsDestroyed.ToString();
			stats_SecondStatValue.text = statsManager.asteroidsHit.ToString();
			stats_ThirdStatValue.text = statsManager.asteroidsEncountered.ToString();
		}
	}

	public void TiltControl(bool tilt){
		if(tilt){
			options_tilt.color = options_tilt_StartColor;

			Color touchTarget = new Color(options_touch_StartColor.r, options_touch_StartColor.g, options_touch_StartColor.b, .5f);
			options_touch.color = touchTarget;
		}
		else{
			options_touch.color = options_touch_StartColor;
			
			Color tiltTarget = new Color(options_tilt_StartColor.r, options_tilt_StartColor.g, options_tilt_StartColor.b, .5f);
			options_tilt.color = tiltTarget;
		}
	}

	public void EnablePostGameStats(){
		postGameText.SetActive(true);
	}

	public void ToggleGameplayGUI(){
		if(scoreTicker.color.a != 0){
			fadeGUI = false;
			scoreTicker.color = new Color(scoreTicker.color.r, scoreTicker.color.g, scoreTicker.color.b, 0);
			laserCounter.color = new Color(laserCounter.color.r, laserCounter.color.g, laserCounter.color.b, 0);
		}
		else{
			fadeGUI = true;
		}
	}
}
