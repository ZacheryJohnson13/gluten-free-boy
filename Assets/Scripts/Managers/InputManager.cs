﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
	public AppManager appManager;
	public GUIManager guiManager;
	public GameplayManager gameplayManager;
	public SoundManager soundManager;

	public RaycastHit2D lastTouchRaycastHit;
	public int counter = 0;	
	
	public bool tiltControlMode;

	void Awake(){
		appManager = GetComponent<AppManager>();
		guiManager = GetComponent<GUIManager>();
		gameplayManager = GetComponent<GameplayManager>();
		soundManager = GetComponent<SoundManager>();
	}

	void Update(){
	#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE)
		if(Input.GetTouch(0).phase == TouchPhase.Began){
			lastTouchRaycastHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y)), Vector2.zero);
			if(lastTouchRaycastHit.collider != null){
				CheckTouchHit();
			}
		}
	#endif

	#if (UNITY_EDITOR || UNITY_WEBPLAYER)
		if(Input.GetKeyDown(KeyCode.Mouse0)){
			lastTouchRaycastHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y)), Vector2.up);
			CheckTouchHit();
		}
	#endif
	
	}

	void CheckTouchHit(){
		counter++;
		switch(lastTouchRaycastHit.transform.name){
		case "Play Button":
			GetComponent<GameplayManager>().MoveCamera(gameplayManager.playCameraPosition);
			gameplayManager.StartGame();
			break;
		case "Stats Button":
			GetComponent<GameplayManager>().MoveCamera(gameplayManager.statsCameraPosition);
			break;
		case "Options Button":
			guiManager.TiltControl(tiltControlMode);
			GetComponent<GameplayManager>().MoveCamera(gameplayManager.optionsCameraPosition);
			break;
		case "How To Play Button":
			GetComponent<GameplayManager>().MoveCamera(gameplayManager.howToPlayCameraPosition);
			break;
		case "Quit Button":
			Application.Quit();
			break;
		case "Return Button":
			GetComponent<GameplayManager>().MoveCamera(gameplayManager.homeCameraPosition);
			break;
		case "Next Category Button":
			guiManager.statMenuCounter++;
			guiManager.UpdateStats();
			break;
		case "Previous Category Button":
			guiManager.statMenuCounter--;
			guiManager.UpdateStats();
			break;
		case "Options Menu - FX Toggle":
			lastTouchRaycastHit.transform.GetComponent<MenuToggle>().Toggle();
			soundManager.UpdateSounds();
			break;
		case "Options Menu - Music Toggle":
			lastTouchRaycastHit.transform.GetComponent<MenuToggle>().Toggle();
			soundManager.UpdateMusic();
			break;
		case "Options Menu - Control Tilt":
			tiltControlMode = true;
			guiManager.TiltControl(tiltControlMode);
			break;
		case "Options Menu - Control Touch":
			tiltControlMode = false;
			guiManager.TiltControl(tiltControlMode);
			break;
		default:
			Debug.Log("Debugging is hell.");
			break;
		}
	}
}