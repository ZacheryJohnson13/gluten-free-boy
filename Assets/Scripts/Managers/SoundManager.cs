﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	public GameObject musicGO;
	public AudioSource[] musicTracks;
	public AudioSource currentTrack;

	public GameObject musicToggleObject;
	public GameObject soundToggleObject;

	public float musicLevel = 1;
	public float soundLevel = 1;


	void Start(){
		musicTracks = musicGO.GetComponents<AudioSource>();
		currentTrack = musicTracks[0];

		UpdateMusic();
		UpdateSounds();
	}

	public void UpdateMusic(){
		if(musicToggleObject.GetComponent<MenuToggle>().on){
			musicLevel = 1;
		}
		else{
			musicLevel = 0;
		}

		currentTrack.volume = musicLevel;
	}

	public void UpdateSounds(){		
		if(soundToggleObject.GetComponent<MenuToggle>().on){
			soundLevel = 1;
		}
		else{
			soundLevel = 0;
		}
	}
}