using UnityEngine;
using System.Collections;

public class GameplayManager : MonoBehaviour {
	public InputManager inputManager;
	public GUIManager guiManager;
	public AppManager appManager;
	public StatsManager statsManager;

	public Camera gameCamera;
	public Character character;
	public DummyCharacter howToPlayDummyCharacter;

	public GameObject[] asteroidPrefabs;
	public GameObject[] powerUpPrefabs;

	public int powerUpFrequency;
	private int powerUpRand;

	public float asteroidSpeed;

	public float tempDifficulty;

	public float score;
	public int lasersShot;
	public int asteroidsDestroyed;

	private int lasersHit;
	private int asteroidsSpawned;

	public int multiplier = 1;

	public float gameTimer;

	public bool gameActive;

	public Vector3 gameCameraInitialPosition;
	public Vector3 gameCameraTargetPosition;

	public Vector3 playCameraPosition = new Vector3(0f, 20f, -10.3f);
	public Vector3 statsCameraPosition = new Vector3(-10f, 1f, -10.3f);
	public Vector3 optionsCameraPosition = new Vector3(10f, 1f, -10.3f);
	public Vector3 howToPlayCameraPosition = new Vector3(0f, -9f, -10.3f);
	public Vector3 homeCameraPosition = new Vector3(0f, 1f, -10.3f);

	void Awake(){
		inputManager = GetComponent<InputManager>();
		guiManager = GetComponent<GUIManager>();
		appManager = GetComponent<AppManager>();
		statsManager = GetComponent<StatsManager>();

		gameCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		gameCameraInitialPosition = gameCamera.transform.position;
		gameCameraTargetPosition = gameCameraInitialPosition;

		Instantiate(character, new Vector3(0, 16, 0), Quaternion.identity);
		AssignCharacter();
	}

	void Update(){		
		//if(gameCamera.transform.position != gameCameraTargetPosition){
		if(Vector3.Distance(gameCamera.transform.position, gameCameraTargetPosition + new Vector3(.1f, .1f)) >= 0.1f || 
		   Vector3.Distance(gameCamera.transform.position, gameCameraTargetPosition - new Vector3(.1f, .1f)) >= 0.1f
		   ){
			gameCamera.transform.position = Vector3.Slerp(gameCamera.transform.position, gameCameraTargetPosition, Time.deltaTime * 2.5f);
		}

		if(gameActive){
			gameTimer += Time.deltaTime;
			score += Time.deltaTime * multiplier * 25;
		}
	}

	public void LevelUp(){
		multiplier++;
		asteroidSpeed = 5 + (1.2f * multiplier);
		character.moveSpeed = 6.5f + (1.5f * multiplier);
	}

	public void AssignCharacter(){
		character = GameObject.FindGameObjectWithTag("Character").GetComponent<Character>();
		howToPlayDummyCharacter = GameObject.FindGameObjectWithTag("Dummy Character").GetComponent<DummyCharacter>();
	}

	public void Score(int points){
		score += points;
		lasersHit++;
	}

	public void StartGame(){
		character.gameObject.SetActive(true);
		howToPlayDummyCharacter.gameObject.SetActive(false);

		LevelUp();
		guiManager.ToggleGameplayGUI();
		multiplier = 1;
		score = 0;
		lasersShot = 0;
		lasersHit = 0;
		asteroidsDestroyed = 0;
		character.isAlive = true;
		character.lasersAvailable = 1;
		character.canShoot = true;
		gameActive = true;
		gameTimer = 0;
		StartCoroutine("AsteroidSpawner");
		StartCoroutine("EnablePostGameStats");
	}

	public void EndGame(){
		multiplier = 1;
	
		character.gameObject.SetActive(false);
		howToPlayDummyCharacter.gameObject.SetActive(true);

		Debug.Log("Ending game.");
		gameActive = false;
		guiManager.ToggleGameplayGUI();

		statsManager.totalScore += Mathf.CeilToInt(score);
		statsManager.gamesPlayed++;
		statsManager.SubmitScore(Mathf.CeilToInt(score));

		statsManager.lasersShot += lasersShot;
		statsManager.lasersHit += lasersHit;

		statsManager.asteroidsDestroyed += lasersHit;
		statsManager.asteroidsHit++;
		statsManager.asteroidsEncountered += asteroidsSpawned;

		statsManager.SaveAllStats();
		statsManager.LoadStats();

		GameObject[] gameplayObjects = GameObject.FindGameObjectsWithTag("Gameplay Object");
		foreach(GameObject gameplayObject in gameplayObjects){
			Destroy(gameplayObject);
		}

		StopAllCoroutines();
		MoveCamera(gameCameraInitialPosition);
		guiManager.UpdateStats();
	}

	public IEnumerator AsteroidSpawner(){
		bool spawn = gameActive;
		while(spawn){
			SpawnAsteroid();
			if(gameTimer > 5){
				tempDifficulty = (Mathf.Abs(2.5f / (1 + Mathf.Exp(gameTimer * .04f))) + .75f);
				yield return new WaitForSeconds(tempDifficulty);
			}
			else{
				yield return new WaitForSeconds(1);
			}
			spawn = gameActive;
		}
	}

	void SpawnAsteroid(){
		float x = Random.Range(-2.75f, 2.75f);
		int asteroid = Random.Range(0, asteroidPrefabs.Length);
		Instantiate(asteroidPrefabs[asteroid], new Vector3(x, 30), Quaternion.identity);
		
		powerUpRand = Random.Range(0, 10000);
		if(powerUpRand > 10000 - (powerUpFrequency * 20)){
			SpawnPowerUp();			
			powerUpFrequency = 50;
		}
		else{
			powerUpFrequency++;
		}
		asteroidsSpawned++;
	}

	void SpawnPowerUp(){
		float x = Random.Range(-2.75f, 2.75f);
		int powerUp = Random.Range(0, powerUpPrefabs.Length);
		Instantiate(powerUpPrefabs[powerUp], new Vector3(x, 30), Quaternion.identity);
	}

	public void MoveCamera(Vector3 targetPosition){
		gameCameraTargetPosition = targetPosition;
	}
	
	IEnumerator EnablePostGameStats(){
		yield return new WaitForSeconds(1);
		guiManager.EnablePostGameStats();
	}
}