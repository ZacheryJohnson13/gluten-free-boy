﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {
	public int moveSpeed;
	public float rotationSpeed;

	public int pointsPerKill;

	private GameplayManager gameplayManager;

	void Start(){
		rotationSpeed = Random.Range(-10.0f, 10.0f);
		gameplayManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>();

		moveSpeed = Mathf.FloorToInt(gameplayManager.asteroidSpeed);
	}

	void Update(){
		transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, transform.position.y - 1), Time.deltaTime * moveSpeed);

		transform.Rotate(0, 0, rotationSpeed);

		if(transform.position.y < 11 || !gameplayManager.gameActive){
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.gameObject.GetComponent<Projectile>()){
			GameObject popUp = (GameObject) Instantiate(gameplayManager.guiManager.popUpText, collider.transform.position, Quaternion.identity) as GameObject;
			popUp.GetComponent<PopUpText>().Set("+" + (pointsPerKill * gameplayManager.multiplier).ToString());
			collider.gameObject.GetComponent<Projectile>().DestroySelf(pointsPerKill);

			Destroy(collider.gameObject);
			Destroy(gameObject);
		}
	}
}
