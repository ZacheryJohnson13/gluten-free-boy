﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	public float moveSpeed;

	void Update(){
		transform.position += moveSpeed * transform.up * Time.deltaTime;

		if(transform.position.y > -4 && transform.position.y < 5 || transform.position.y > 25.5f){
			Destroy(gameObject);
		}
	}

	public void DestroySelf(int points){		
		GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().Score(points);		
		GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().asteroidsDestroyed++;
		Destroy(gameObject);
	}
}
