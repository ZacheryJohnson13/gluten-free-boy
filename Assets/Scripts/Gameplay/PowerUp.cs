﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {
	public int moveSpeed;

	public void Update (){
		transform.position += new Vector3(0, -Time.deltaTime * moveSpeed, 0);

		gameObject.transform.localScale += new Vector3(Mathf.Sin(Time.time * 8)/450, Mathf.Sin(Time.time * 8)/450);

		if(transform.position.y < 11){
			Destroy(gameObject);
		}
	}
}
