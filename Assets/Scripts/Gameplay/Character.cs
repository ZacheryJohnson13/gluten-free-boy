﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {
	public InputManager inputManager;

	public GameObject lazerPrefab;
	public Vector3 leftEyePosition;
	public Vector3 rightEyePosition;

	public int lasersAvailable = 1;
	public bool canShoot = true;

	private bool canMove = true;

	public GameObject characterShieldPrefab;
	private GameObject currentShield;
	private bool canDie = true;

	public float moveSpeed;
	private float currentMoveSpeed;

	public float tiltSensitivity;
	private float zRotOffset;

	public bool isAlive = true;
	private bool useAccelerometer = false;

	private Vector3 accel;

	void Awake(){		
		inputManager = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<InputManager>();
	}

	void Start(){
		accel = Input.acceleration;
	}

	void FixedUpdate(){		
		currentMoveSpeed = moveSpeed;

		leftEyePosition = transform.FindChild("Left Eye").position;
		rightEyePosition = transform.FindChild("Right Eye").position;

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,0), Time.deltaTime * 12);
		tiltSensitivity = Mathf.Clamp(tiltSensitivity, 0, 1);

		#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE)
		useAccelerometer = inputManager.tiltControlMode;
		if(useAccelerometer){
			accel = Vector3.Slerp(accel, Input.acceleration, 10 * Time.deltaTime);
		}

		if(inputManager.gameplayManager.gameActive){
			if(useAccelerometer){
				if(accel.x > .05f * tiltSensitivity && accel.x < .15f * tiltSensitivity){
					Move(Vector2.right * .5f);
					currentMoveSpeed = moveSpeed/2;
				}
				
				else if(accel.x > .15f * tiltSensitivity){
					Move(Vector2.right);
					currentMoveSpeed = moveSpeed;
				}
				
				if(accel.x < -.05f * tiltSensitivity && accel.x > -.15f * tiltSensitivity){
					Move(-Vector2.right * .5f);
					currentMoveSpeed = moveSpeed/2;
				}
				else if(accel.x < -.15f * tiltSensitivity){
					Move(-Vector2.right);
					currentMoveSpeed = moveSpeed;
				}

				if(Input.GetTouch(Input.touchCount-1).phase == TouchPhase.Began){
					if(lasersAvailable > 0 && canShoot){
						StartCoroutine(ShootLasers());
					}
				}
			}
			else{
				if(Input.GetTouch(Input.touchCount-1).position.x <= (Screen.width * .5f) && Input.GetTouch(Input.touchCount-1).position.y > (Screen.height * .075f)){ //Left Section
					Move(-Vector2.right);
				}
				else if(Input.GetTouch(Input.touchCount-1).position.x > (Screen.width * .5f) && Input.GetTouch(Input.touchCount-1).position.y > (Screen.height * .075f)){ //Right Section
					Move(Vector2.right);
				}
				if(Input.GetTouch(Input.touchCount-1).position.y < (Screen.height * .1f)){ //Above bottom of screen
					if(lasersAvailable > 0 && canShoot){
						StartCoroutine(ShootLasers());
					}
				}
			}
		}
		#endif
		//Put code above Input stuff, some code won't work below it.

		#if (UNITY_EDITOR || UNITY_WEBPLAYER)
		if(Input.GetKey(KeyCode.RightArrow)){
			Move(Vector2.right);
		}

		else if(Input.GetKey(KeyCode.LeftArrow)){
			Move(-Vector2.right);
		}

		if(Input.GetKey(KeyCode.Space)){
			if(lasersAvailable > 0 && canShoot){
				StartCoroutine(ShootLasers());
			}
		}
		#endif
	}

	/// <summary>
	/// Move the character to either the left or right.
	/// </summary>
	/// <param name="direction">Vector2.right would move the character to the right; -Vector2.right would move the character to the left.</param>
	public void Move(Vector2 direction){
		if(canMove){
			transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x + direction.x, transform.position.y + direction.y), Time.deltaTime * currentMoveSpeed);
			transform.RotateAround(transform.position, Vector3.forward, -direction.x * 12);
		}

		if(transform.position.x > 2.5f){
			transform.position = Vector2.MoveTowards(transform.position, new Vector2(2.5f, transform.position.y), 1000);
		}
		if(transform.position.x < -2.5f){
			transform.position = Vector2.MoveTowards(transform.position, new Vector2(-2.5f, transform.position.y), 1000);
		}
	}

	public IEnumerator ShootLasers(){
		canShoot = false;
		Debug.Log("Shooting lazers");

		Instantiate(lazerPrefab, leftEyePosition, transform.rotation);
		Instantiate(lazerPrefab, rightEyePosition, transform.rotation);

		lasersAvailable--;
		GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().lasersShot += 2;
		
		yield return new WaitForSeconds(0.5f);
		canShoot = true;
	}

	public IEnumerator GainShield(){
		if(currentShield != null){
			RemoveShield(currentShield);
		}
		GameObject shield;	
		shield = (GameObject) Instantiate(characterShieldPrefab, transform.position, transform.rotation) as GameObject;
		shield.transform.parent = this.transform;
		currentShield = shield;
		canDie = false;
		yield return new WaitForSeconds(5.0f);
		RemoveShield(currentShield);
	}

	public void RemoveShield(GameObject currentShield){	
		canDie = true;
		Destroy(currentShield);
	}

	public void EnableMovement(){
		canMove = true;
	}

	public void DisableMovement(){
		canMove = false;
	}

	public void Die(){
		GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameplayManager>().EndGame();
		isAlive = false;
	}

	public void OnTriggerEnter2D(Collider2D collider){
		if(collider.gameObject.GetComponent<JuiceBox>()){
			lasersAvailable++;
			Destroy(collider.gameObject);
		}
		if(collider.gameObject.GetComponent<Multiplier>()){
			inputManager.gameplayManager.LevelUp();
			GameObject popUp = (GameObject) Instantiate(inputManager.gameplayManager.guiManager.popUpText, collider.transform.position, Quaternion.identity) as GameObject;
			popUp.GetComponent<PopUpText>().Set((inputManager.gameplayManager.multiplier.ToString() + "x").ToString());
			Destroy(collider.gameObject);
		}
		if(collider.gameObject.GetComponent<Shield>()){
			StopCoroutine("GainShield");
			StartCoroutine("GainShield");
			Destroy(collider.gameObject);
		}
	}
		
	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.GetComponent<Asteroid>()){
			Destroy(collision.gameObject);
			if(canDie){
				Die();
			}
		}
	}
}